
# Maquettes version en ligne
https://www.figma.com/file/xUyDUxaSt1Z4JcG083fD32/Untitled?node-id=0%3A1

# Maquettes version en pdf
Voire "Pétanque.pdf" dans le repo git

# La version consultable en ligne du projet 

https://tqdxc1991.gitlab.io/roule-ta-boule

petanque-qiong.surge.sh


# Une notice d'installation du projet

Vous pouvez telecharger le dossier en format zip et les extraire dans votre local, ou faites un fork si vous avec un compte gitlab et puis faites un gitclone pour cloner dans votre local.

Vous pouvez ouvir le dossier index.html pour voire le site en local.

# problème rencontré

background image si url enligne, chargement pas possible en mobile
donc télecharger en racine, comme ça pas de problem en mobile

# Contexte du projet

Le club sportif "roule ta boule" organise un grand concours régional de pétanque et souhaite diffuser l'information sur les réseaux sociaux, sous la forme d'une unique page web qui pourra être partagée facilement.

Le concours se déroulera à Carmaux le 12 juin prochain. Les participants devront s'inscrire au minimum un mois avant l'événement en envoyant un message à rouletaboule@cornomail.net et préciser les équipes complètes (2 ou 4 personnes majeures obligatoirement), ainsi que leur club d'affiliation, s'il en sont adhérant.

La charte graphique est assez libre : le projet doit être sur une base de vert et marron avec une police sans empattement. Le noir est interdit pour les textes, et une illustration doit être inclue. Bien entendu, la création doit être absolument magnifique et donner une envie irrésistible de venir à l'événement, qu'on soit joueur ou non :)

L'engagement est fixé à 10€ pour les licenciés, et 15€ pour les autres. La logistique sur place permettra de se restaurer et se désaltérer. Un espace sanitaire sera à disposition. Pas de distributeur de billet sur place.

# Critères de performance

Compétence 1 : Les maquettes sont au format numérique et font apparaitre l'ensemble des éléments présentés dans la demande client. Elles mettent en évidence l'adaptabilité du projet à tous types de supports numériques.

Compétence 2 : Les pages sont conformes aux maquettes, respectent la charte graphique imposée, ainsi que les bonnes pratiques en terme d'éco-conception, de performance et sécurité :

- ecoindex B minimum (ecoindex.fr)
- performance score 80% minimum (PageSpeed Insight, mobile ET ordinateur)
- accessibilité correcte (aucune erreur détectée par l'extension de navigateur Wave)
- indentation propre

A ces critères s'ajoutent les critères de performance du REAC pour les compétences ciblées.
